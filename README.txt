SUMMARY
-------

The Sympa Subscribe module provides a block for subscribing to a Sympa mailing
list.

See https://github.com/blairc/sympa_subscribe/tree/7.x-1.x for more information
on the Drupal 7.x module.


AUTHOR
------

Blair Christensen | blair.christensen@gmail.com
https://github.com/blairc/sympa_subscribe

